from aiogram import Bot, Dispatcher, executor, types
from aiogram.utils.markdown import hbold, hlink
from aiogram.dispatcher.filters import Text
from aiogram.types import ReplyKeyboardMarkup, KeyboardButton
import spyder_for_pars as sfp


# Кнопки для меню tg bot
btn_knife = KeyboardButton('Ножи')
btn_knife_350_500 = KeyboardButton('350-500')
btn_knife_900_1000 = KeyboardButton('900-1000')

btn_gloves = KeyboardButton('Перчатки')
btn_gloves_200_1000 = KeyboardButton('200-1000')

btn_awp = KeyboardButton('AWP')
btn_awp_200_1000 = KeyboardButton('700-850')

btn_main = KeyboardButton('Главное меню')
btn_parser = KeyboardButton('Парсер')

parser_menu = types.ReplyKeyboardMarkup(resize_keyboard=True).add(btn_knife, btn_gloves, btn_awp, btn_main)
knifes_menu = types.ReplyKeyboardMarkup(resize_keyboard=True).add(btn_knife_350_500, btn_knife_900_1000, btn_main)
main_menu = types.ReplyKeyboardMarkup(resize_keyboard=True).add(btn_parser)
gloves_menu = types.ReplyKeyboardMarkup(resize_keyboard=True).add(btn_gloves_200_1000, btn_main)
awp_menu = types.ReplyKeyboardMarkup(resize_keyboard=True).add(btn_awp_200_1000, btn_main)


# Класс Воt может принимать 3 аргумента, token, session, parse_mode
bot = Bot(token='your_token', parse_mode=types.ParseMode.HTML)

# Dispatcher для взаимодействия с Routers или Handler
dp = Dispatcher(bot)

# Decorator , обёртка для функции/ async and await, ассинхронный код, await - ока жду, сделаю что нибудь еще
@dp.message_handler(commands=["start"])
async def start_message(message):
    await message.answer('Lets start?', reply_markup=main_menu)


@dp.message_handler()
async def get_knives(message: types.Message):
    if message.text == 'Главное меню':
        await bot.send_message(message.from_user.id, 'Главное меню', reply_markup=main_menu)
    elif message.text == 'Парсер':
        await bot.send_message(message.from_user.id, 'Парсер', reply_markup=parser_menu)

    elif message.text == 'Ножи':
        await bot.send_message(message.from_user.id, 'Ножи', reply_markup=knifes_menu)
    elif message.text == '350-500':
        await message.answer('Начинаю собирать инфу по ножам 350-500$ \nЭто займет какое то время...')
        differents = sfp.collect_items(350, 500, -22, types_items='order=asc&sort=price&type=2')
        for dif in differents:
            card = (f'{hlink(dif["LINK"], dif["LINK"])}\n'
                    f'{hbold("1.")} {dif["STEAM"]}\n'
                    f'{hbold("2.")} {dif["CS_MONEY"]}\n'
                    f'{hbold("3.")} {dif["DIFFERENT"]}\n')
            await message.answer(card)
    elif message.text == '900-1000':
        await message.answer('Начинаю собирать инфу по ножам 900-1000$ \nЭто займет какое то время...')
        differents = sfp.collect_items(900, 1000, -22, types_items='order=asc&sort=price&type=2')
        for dif in differents:
            card = (f'{hlink(dif["LINK"], dif["LINK"])}\n'
                    f'{hbold("1.")} {dif["STEAM"]}\n'
                    f'{hbold("2.")} {dif["CS_MONEY"]}\n'
                    f'{hbold("3.")} {dif["DIFFERENT"]}\n')
            await message.answer(card)

    elif message.text == 'Перчатки':
        await bot.send_message(message.from_user.id, 'Перчатки', reply_markup=gloves_menu)
    elif message.text == '200-1000':
        await message.answer('Начинаю собирать инфу по перчаткам 200-1000$ \nЭто займет какое то время...')
        differents = sfp.collect_items(200, 1000, -21, types_items='sort=botFirst&type=13')
        for dif in differents:
            card = (f'{hlink(dif["LINK"], dif["LINK"])}\n'
                    f'{hbold("1.")} {dif["STEAM"]}\n'
                    f'{hbold("2.")} {dif["CS_MONEY"]}\n'
                    f'{hbold("3.")} {dif["DIFFERENT"]}\n')
            await message.answer(card)

    elif message.text == 'AWP':
        await bot.send_message(message.from_user.id, 'AWP', reply_markup=awp_menu)
    elif message.text == '700-850':
        await message.answer('Начинаю собирать инфу по AWP 700-850$ \nЭто займет какое то время...')
        differents = sfp.collect_items(700, 850, -22, types_items='order=asc&sort=price&type=4')
        # await message.answer('Please waiting...', reply_markup=keyboard)
        for dif in differents:
            card = (f'{hlink(dif["LINK"], dif["LINK"])}\n'
                    f'{hbold("1.")} {dif["STEAM"]}\n'
                    f'{hbold("2.")} {dif["CS_MONEY"]}\n'
                    f'{hbold("3.")} {dif["DIFFERENT"]}\n')
            await message.answer(card)
    else:
        await message.reply('Некоректная команда')


def main():
    # Запуск и передача настроек боту
    executor.start_polling(dp)


if __name__ == '__main__':
    main()