from fake_useragent import UserAgent
from knives_gloves_other import collect_data_market_cs
import requests

ua = UserAgent()


def collect_items(min_price=500, max_price=900, oversize_cs=-20, types_items='order=asc&sort=price&type=2'):
    offset = 0
    batch_size = 60
    result = []
    count = 0

    while True:
        # Делаем шаг оф сета в 60, получаем по 60 предметов, нужно для коректой работы ссылки HTML
        for item in range(offset, offset + batch_size, 60):
            url = f'https://inventories.cs.money/5.0/load_bots_inventory/730?buyBonus=35&isStore=true&limit=60&maxPrice={max_price}&minPrice={min_price}&offset={item}&{types_items}&withStack=true'
            response = requests.get(
                url=url,
                headers={'user-agent': f'{ua.random}'},
            )

            offset += batch_size

            data = response.json()

            if data.get('error') == 2:
                print('END')
                differents = collect_data_market_cs(result)
                return differents

            items = data.get('items')

            # С каждой страницы берем 60 предметов
            for i in items:
                # Если скидка > , получаем из предмета значения/
                # 'Phase' not in i.get('fullName') убираем все ножи с Phase
                if i.get('overprice') is not None and i.get('overprice') < oversize_cs and 'Phase' not in i.get('fullName'):
                    item_full_name = i.get('fullName')
                    item_3d = i.get('3d')
                    item_price = i.get('price')
                    item_over_price = i.get('overprice')

                    # Записываем значения предмета в список
                    result.append(
                        {
                            'full_name': item_full_name,
                            '3d': item_3d,
                            'item_price': item_price,
                            'overprice': item_over_price,
                        }
                    )
        # Увеличиваем итерацию на 1(Получаем следующие 60 предметов на новой динамической странице)
        count += 1
        print(f' Page #{count}')
        print(url)

        # Показать количество найденных предметов
        print(len(result))
