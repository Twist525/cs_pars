import requests
from fake_useragent import UserAgent
from bs4 import BeautifulSoup
import time
import random

ua = UserAgent()


def collect_data_market_cs(result):
    differents = []
    for item in result:
        # Делаем запрос по предмету для поисковой страницы Steam
        utl_piece_with_base = item['full_name']
        utl_piece_with_base = utl_piece_with_base.replace(' ', '+')
        utl_piece_with_base = utl_piece_with_base.replace('|', '%7C')
        utl_piece_with_base = utl_piece_with_base.replace('(', '%28')
        utl_piece_with_base = utl_piece_with_base.replace(')', '%29')
        url = f'https://steamcommunity.com/market/search?q={utl_piece_with_base}'

        # В data передаем словарь, ключ - id с нужным значение поля в HTML, значение - текст который передаем
        response = requests.get(
            url=url,
            headers={'user-agent': f'{ua.random}'},
        )

        # Получаем странице в формате HTML
        soup = BeautifulSoup(response.text, 'html.parser')

        # Найдем значение цен для предмета
        try:
            price_now = soup.find('span', class_="normal_price")
            # Найдем имя
            name_now = soup.find('span', class_='market_listing_item_name')
            price_steam = str(price_now.text).split()[2][1:]
            price_money = item['item_price']
            # print(f"CS_MONEY(⬇): {item['full_name']} {price_money}")
            # print(f'STEAM(⬆)   : {name_now.text} {price_steam}')

            different = float(price_steam) - float(price_money)
            differents.append({
                'CS_MONEY': f"CS_MONEY: {item['full_name']} {price_money}",
                'STEAM': f'STEAM   : {name_now.text} {price_steam}',
                'DIFFERENT': f'Разница: {different}',
                'LINK': item['3d'],
            },)
            print('+1')
        except:
            print('Данного предмета в стиме нет!')
        time.sleep(random.randrange(3, 8))
    print('Предметы кончились')
    return differents